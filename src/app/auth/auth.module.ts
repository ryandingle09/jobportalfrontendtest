import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route } from './route.module';
import { AuthComponent } from './auth.component';
import { NoAuthGuard } from '../shared/services/noAuth.guard';

import { 
  MatFormFieldModule, 
  MatInputModule,
  MatButtonModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    Route,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AuthComponent],
  providers: [NoAuthGuard]
})
export class AuthModule { }
