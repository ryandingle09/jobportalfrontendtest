import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { HeaderComponent } from '../shared/layout/header/header.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers: [HeaderComponent]
})
export class AuthComponent implements OnInit {

  @Output() UpdateSession: EventEmitter<any> = new EventEmitter();

  public form = new FormGroup({
    'email': new FormControl(''),
    'password': new FormControl(''),
  });

  constructor(private router: Router, private auth: AuthService, private _snackBar: MatSnackBar, private header: HeaderComponent) { }

  onSubmit() {
      let data: FormData = new FormData();

      data.append('email', this.form.value.email == null ? '' : this.form.value.email);
      data.append('password', this.form.value.password == null ? '' : this.form.value.password);

      this.auth.login(data).subscribe(resp => {

        let res: any = resp;

        this._snackBar.open(res.status, res.message, {
          duration: 4000,
        });

        this.auth.setLogin(res.data, res.token);
        
        //this.header.UpdateLinks();
        this.UpdateSession.emit();

        // havigate to admin
        if(res.data.user_type == '1') {
          this.router.navigate(['/admin/users'])
        }
        else {
          this.router.navigate(['/admin/employers'])
        }
      },err => {
        this._snackBar.open(err.error.message, err.error.status, {
          duration: 4000,
        });
      });
  }

  ngOnInit() {
  }

}
