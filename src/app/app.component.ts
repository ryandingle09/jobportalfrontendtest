import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Job Portal';
  isLogin = this.auth.isLoggedIn();
  userData = this.auth.getUserToken();
  token = this.auth.getToken();

  constructor(private auth: AuthService, private router: Router) {
    router.events.subscribe((val) => {
      this.isLogin = this.auth.isLoggedIn();
      this.userData = this.auth.getUserToken();
      this.token = this.auth.getToken();
    });
  }
}
