import { Component, OnInit } from '@angular/core';
import {JobModel } from '../../shared/models';
import { JobService } from '../../shared/services/job.service';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
  public jobs: JobModel[] = [];

  constructor(private job: JobService) { }

  ngOnInit() {
    this.job.all().subscribe(data => {
        this.jobs = data
      }
    );
  }

}
