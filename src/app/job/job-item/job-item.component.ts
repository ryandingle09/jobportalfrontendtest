import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { JobService } from '../../shared/services/job.service';
import { ApplicantService } from '../../shared/services/applicant.service';
import { Errors } from '../../shared/models';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.css']
})

export class JobItemComponent implements OnInit {

  public fileValue: any;
  public errors: Errors;
  public id: any = this.route.params['value']['id'];
  public title: string;
  public description: string;

  public ApplyForm = new FormGroup({
    'email': new FormControl('', [Validators.required, Validators.email]),
    'fullname': new FormControl('', [Validators.required]),
    'contact': new FormControl('', [Validators.required]),
    'resume': new FormControl('',[Validators.required, RxwebValidators.extension({extensions:["pdf","docx","doc","dot","dotx"]})]), 
  });

  constructor(private app: ApplicantService, private job: JobService, private route: ActivatedRoute, private _snackBar: MatSnackBar) {}

  onSubmit() {
    if (this.ApplyForm.valid) {
      let data: FormData = new FormData();

      data.append('email', this.ApplyForm.value.email == null ? '' : this.ApplyForm.value.email);
      data.append('full_name', this.ApplyForm.value.fullname == null ? '' : this.ApplyForm.value.fullname);
      data.append('contact', this.ApplyForm.value.contact == null ? '' : this.ApplyForm.value.contact);
      data.append('resume',  this.fileValue !== undefined ? this.fileValue : '');
      data.append('job_id',  this.id);

      this.app.apply(data).subscribe(res => {
        this.ApplyForm.reset();
        this.ApplyForm.controls.email.setErrors(null);
        this.ApplyForm.controls.fullname.setErrors(null);
        this.ApplyForm.controls.contact.setErrors(null);
        this.ApplyForm.controls.resume.setErrors(null);

        this._snackBar.open(res.message, res.status, {
          duration: 4000,
        });
      });
    }
  }

  onFileChange(event) {
    this.fileValue = event.target.files[0];
  }

  ngOnInit() {
    this.job.get(this.id).subscribe(data => {
        this.title = data.title
        this.description = data.description
      }
    );
  }

}
