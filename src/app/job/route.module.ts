import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobItemComponent } from './job-item/job-item.component';
import { JobListComponent } from './job-list/job-list.component';

const routes: Routes = [
  {
    path: 'jobs',
    component: JobListComponent,
  },
  {
    path: 'job/post/:id',
    component: JobItemComponent,
    // children: [
    //   { path: '', component: JobItemComponent }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Route { }
