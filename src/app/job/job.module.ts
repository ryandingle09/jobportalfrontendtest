import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from "@rxweb/reactive-form-validators";
import { Route } from './route.module';
import { JobItemComponent } from './job-item/job-item.component';
import { JobListComponent } from './job-list/job-list.component';
import { HttpClientModule }    from '@angular/common/http';
import { JobService } from '../shared/services/job.service';

import { 
  MatCardModule, 
  MatButtonModule, 
  MatPaginatorModule, 
  MatFormFieldModule, 
  MatSnackBarModule,
  MatInputModule
} from '@angular/material';

@NgModule({
  declarations: [
    JobItemComponent,
    JobListComponent,
  ],
  imports: [
    CommonModule,
    Route,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    RxReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [JobService]
})
export class JobModule { }
