import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
  })
export class StorageService {

    public get(name) {
        return JSON.parse(localStorage.getItem(name));
    }

    public status(name) {
        return localStorage.getItem(name) ? true : false;
    }

    public set(name, data) {
        this.remove(name);
        return localStorage.setItem(name, JSON.stringify(data));
    }

    public remove(name) {
        return localStorage.removeItem(name);
    }
}