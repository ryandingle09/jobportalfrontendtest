import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models';

@Injectable({
  providedIn: 'root',
})
export class EmployerService {

    private url = 'http://jobportal.test/api/v1/user'; 

    constructor( private http: HttpClient) { }
    
    all(): Observable<UserModel[]> {
        return this.http.get<UserModel[]>(this.url+'/list?user_type=2').pipe();
    }

    get(id: number): Observable<UserModel> {
        const url = `${this.url}/post?id=${id}`;
        return this.http.get<UserModel>(url).pipe();
    }

    search(term: string): Observable<UserModel[]> {
        return this.http.get<UserModel[]>(`${this.url}/list?user_type=2&search=${term}`).pipe();
    }

    add(data){
        return this.http.post(this.url+'/add', data).pipe();
    }

    delete(id: number): Observable<UserModel> {
        const url = `${this.url}/delete?id=${id}`;
        return this.http.get<UserModel>(url).pipe();
    }

    update(data){
        return this.http.post(this.url+'/update', data).pipe();
    }
}