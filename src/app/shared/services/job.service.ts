import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JobModel } from '../models';

@Injectable({
  providedIn: 'root',
})
export class JobService {

    private url = 'http://jobportal.test/api/v1/job'; 

    constructor( private http: HttpClient) { }
    
    all(): Observable<JobModel[]> {
        return this.http.get<JobModel[]>(this.url+'/list').pipe();
    }

    get(id: number): Observable<JobModel> {
        const url = `${this.url}/post?id=${id}`;
        return this.http.get<JobModel>(url).pipe();
    }

    delete(id: number): Observable<JobModel> {
        const url = `${this.url}/delete?id=${id}`;
        return this.http.get<JobModel>(url).pipe();
    }

    search(term: string): Observable<JobModel[]> {
        return this.http.get<JobModel[]>(`${this.url}/list?search=${term}`).pipe();
    }

    add(data): Observable<JobModel> {
        return this.http.post<JobModel>(this.url+'/add', data).pipe();
    }

    update(data): Observable<any> {
        return this.http.post(this.url+'/update', data).pipe();
    }
}