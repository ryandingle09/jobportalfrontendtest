import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApplicantModel } from '../models';

@Injectable({
  providedIn: 'root',
})
export class ApplicantService {

    private url = 'http://jobportal.test/api/v1/applicant'; 

    constructor( private http: HttpClient) { }
    
    all(): Observable<ApplicantModel[]> {
        return this.http.get<ApplicantModel[]>(this.url+'/list').pipe();
    }

    get(id: number): Observable<ApplicantModel> {
        const url = `${this.url}/get?id=${id}`;
        return this.http.get<ApplicantModel>(url).pipe();
    }

    delete(id: number): Observable<ApplicantModel> {
        const url = `${this.url}/delete?id=${id}`;
        return this.http.get<ApplicantModel>(url).pipe();
    }

    search(term: string): Observable<ApplicantModel[]> {
        return this.http.get<ApplicantModel[]>(`${this.url}/list?search=${term}`).pipe();
    }

    apply(data): Observable<ApplicantModel> {
        return this.http.post<ApplicantModel>(this.url+'/apply', data).pipe();
    }
}