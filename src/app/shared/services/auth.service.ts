import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private islogin   = localStorage.getItem('token') ? true : false;
  private url = 'http://jobportal.test/api/v1/auth'; 

  constructor( private http: HttpClient) { }

  login(data){
    return this.http.post(this.url+'/login', data).pipe();
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getUserToken() {
    return JSON.parse(localStorage.getItem('user'));
  }

  setLogin(user, token) {
    this.islogin = true;
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
  }

  setLogout() {
    this.islogin = false;
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  isLoggedIn() {
    return this.islogin;
  }
}