export class JobModel {
	data: any;
	total: number;
	status: any;
	message: any;
	id: any;
	title: any;
    description: any;
    employer_id: any;
	created_at: any;
	updated_at: any;
}