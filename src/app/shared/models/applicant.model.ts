export class ApplicantModel {
	data: any;
	total: number;
	status: any;
	message: any;
	id: any;
	fullname: any;
	full_name: any;
    email: any;
    contact: any;
	resume: any;
	job_id:any;
	created_at: any;
	updated_at: any;
}