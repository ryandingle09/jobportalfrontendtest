export class UserModel {
	data: any;
	total: number;
	id: any;
	status: any;
	message: any;
	first_name: any;
	last_name: any;
	email: any;
	user_type: any;
	created_at: any;
	updated_at: any;
}