import { Component, OnInit, Input, Output } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input('userData') userData: any;
  @Input('isLogin') isLogin: any;
  @Input('token') token: any;

  title = 'Job Portal';

  constructor(private auth: AuthService, private route: Router) { }

  logout() {
    this.auth.setLogout();
    this.route.navigateByUrl('/');
    this.UpdateLinks();
  }

  UpdateLinks() {
    this.isLogin = this.auth.isLoggedIn();
    this.userData = this.auth.getUserToken();
    this.token = this.auth.getToken();
  }

  ngOnInit() {
    this.UpdateLinks();
  }

}