import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from "@rxweb/reactive-form-validators"
import { Route } from './route.module';
import { EmployerListComponent } from './employer-list/employer-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { EmployerItemComponent } from './employer-item/employer-item.component';
import { UserItemComponent } from './user-item/user-item.component';
import { HttpClientModule }    from '@angular/common/http';
import { JobItemComponent } from './job-item/job-item.component';
import { JobListComponent } from './job-list/job-list.component';
import { ApplicantListComponent } from './applicant-list/applicant-list.component';
import { ApplicantItemComponent } from './applicant-item/applicant-item.component';

import { 
  MatTableModule,
  MatFormFieldModule, 
  MatInputModule,
  MatCardModule, 
  MatButtonModule, 
  MatPaginatorModule, 
  MatSnackBarModule,
  MatOptionModule,
  MatSelectModule,
  MatChipsModule
} from '@angular/material';

import { UserService } from '../shared/services/user.service';
import { JobService } from '../shared/services/job.service';
import { ApplicantService } from '../shared/services/applicant.service';
import { AuthGuard } from '../shared/services/auth.guard';

@NgModule({
  declarations: [
      EmployerListComponent,
      UserListComponent,
      JobItemComponent,
      JobListComponent,
      ApplicantItemComponent,
      ApplicantListComponent,
      EmployerItemComponent,
      UserItemComponent
    ],
  imports: [
    CommonModule,
    Route,
    MatTableModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule, 
    MatPaginatorModule, 
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    RxReactiveFormsModule,
    MatChipsModule
  ],
  providers: [UserService, JobService, ApplicantService, AuthGuard]
})
export class AdminModule { }
