import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'app-user-item',
  templateUrl: './employer-item.component.html',
  styleUrls: ['./employer-item.component.css']
})
export class EmployerItemComponent implements OnInit {

  public ListShow = true;
  public id: any = this.route.params['value']['id'];
  public email: string;
  public first_name: string;
  public last_name: string;
  public created_at: string;

  public form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    first_name: new FormControl('', [Validators.required]),
    last_name: new FormControl('', [Validators.required]),
    password: new FormControl('',[Validators.required, Validators.minLength(6)]),
    confirm_password: new FormControl('',[Validators.required, RxwebValidators.compare({fieldName:'password'})]),
  });

  constructor(private user: UserService, private route: ActivatedRoute, private _snackBar: MatSnackBar, private auth: AuthService) {}

  getData() {
    this.user.get(this.id).subscribe(data => {
      this.email = data.email
      this.first_name = data.first_name
      this.last_name = data.last_name
      this.created_at = data.created_at
      this.form.patchValue(data)
    });
  }

  showForm() {
    this.ListShow = false;
  }

  closeForm() {
    this.ListShow = true;
    this.form.reset();
    this.getData();
  }

  onSubmit() {
    if (this.form.valid) {

      let data: FormData = new FormData();

      data.append('email', this.form.value.email == null ? '' : this.form.value.email);
      data.append('email_old', this.email);
      data.append('first_name', this.form.value.first_name == null ? '' : this.form.value.first_name);
      data.append('last_name', this.form.value.last_name == null ? '' : this.form.value.last_name);
      data.append('user_type',  '2');
      data.append('password',  this.form.value.password == null ? '' : this.form.value.password);
      data.append('confirm_password',  this.form.value.confirm_password == null ? '' : this.form.value.confirm_password);
      data.append('id',  this.id);

      this.user.update(data).subscribe(res => {

        this.getData();

        let err:any = res;

        this._snackBar.open(err.message, err.status, {
          duration: 4000,
        });

        this.closeForm();
      },err => {
        this._snackBar.open(err.error.message, err.error.status, {
          duration: 4000,
        });
      });
    }
  }

  ngOnInit() {
    this.getData();
  }

}
