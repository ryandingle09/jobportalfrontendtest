import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployerListComponent } from './employer-list/employer-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { EmployerItemComponent } from './employer-item/employer-item.component';
import { UserItemComponent } from './user-item/user-item.component';
import { ApplicantListComponent } from './applicant-list/applicant-list.component';
import { ApplicantItemComponent } from './applicant-item/applicant-item.component';
import { JobListComponent } from './job-list/job-list.component';
import { JobItemComponent } from './job-item/job-item.component';
import { AuthGuard } from '../shared/services/auth.guard';

const routes: Routes = [
  {
    path: 'admin/users',
    component: UserListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/user/details/:id',
    component: UserItemComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/employers',
    component: EmployerListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/employer/details/:id',
    component: EmployerItemComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/jobs',
    component: JobListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/job/details/:id',
    component: JobItemComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/applicants',
    component: ApplicantListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/applicant/details/:id',
    component: ApplicantItemComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Route { }
