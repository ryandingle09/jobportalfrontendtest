import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApplicantModel } from '../../shared';;
import { ApplicantService } from '../../shared/services/applicant.service';

@Component({
  selector: 'app-applicant-list',
  templateUrl: './applicant-list.component.html',
  styleUrls: ['./applicant-list.component.css']
})
export class ApplicantListComponent implements OnInit {

  public ListShow = true;
  public dataSource: ApplicantModel[] = [];
  public displayedColumns: any[] = ['id', 'full_name', 'email', 'created_at', 'action'];

  constructor(private app: ApplicantService) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.app.search(filterValue).subscribe(res => {
      this.dataSource = res;
    });
  }

  getData() {
    this.app.all().subscribe(res => {
      this.dataSource = res;
    });
  }

  delete(id) {
    this.app.delete(id).subscribe(res => {
      this.getData();
    });
  }

  ngOnInit() {
    this.getData();
  }

}

