import { Component, OnInit } from '@angular/core';
import { JobService } from '../../shared/services/job.service';
import { AuthService } from '../../shared/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.css']
})

export class JobItemComponent implements OnInit {
  public ListShow = true;
  public id: any = this.route.params['value']['id'];
  public title: string;
  public description: string;
  public created_at: string;
  public user_id = this.auth.getUserToken();

  public form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  });

  constructor(private job: JobService, private route: ActivatedRoute, private _snackBar: MatSnackBar, private auth: AuthService) {}

  getData() {
    this.job.get(this.id).subscribe(data => {
      this.title = data.title
      this.description = data.description
      this.created_at = data.created_at
      this.form.patchValue(data)
    });
  }

  showForm() {
    this.ListShow = false;
  }

  closeForm() {
    this.ListShow = true;
    this.form.reset();
    this.getData();
  }

  onSubmit() {
    if (this.form.valid) {

      let data: FormData = new FormData();

      data.append('title', this.form.value.title == null ? '' : this.form.value.title);
      data.append('description', this.form.value.description == null ? '' : this.form.value.description);
      data.append('employer_id', this.user_id.id); //change value to logged user employer
      data.append('id', this.id); //change value to logged user employer

      this.job.update(data).subscribe(res => {

        this.getData();

        this._snackBar.open(res.message, res.status, {
          duration: 4000,
        });

        this.closeForm();
      },err => {
        this._snackBar.open(err.error.message, err.error.status, {
          duration: 4000,
        });
      });
    }
  }

  ngOnInit() {
    this.getData();
  }

}
