import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserModel } from '../../shared';;
import { UserService } from '../../shared/services/user.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {

  public formUser = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    first_name: new FormControl('', [Validators.required]),
    last_name: new FormControl('', [Validators.required]),
    password: new FormControl('',[Validators.required, Validators.minLength(6)]),
    confirm_password: new FormControl('',[Validators.required, RxwebValidators.compare({fieldName:'password'})]),
  });

  public ListShow = true;
  public dataSource: UserModel[] = [];
  public displayedColumns: any[] = ['id', 'first_name', 'last_name', 'email', 'created_at', 'updated_at', 'action'];

  constructor(private user: UserService, private _snackBar: MatSnackBar) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.user.search(filterValue).subscribe(res => {
      this.dataSource = res;
    });
  }

  showForm() {
    this.ListShow = false;
    this.onReset();
  }

  closeForm() {
    this.ListShow = true;
    this.getData();
  }

  onSubmit() {
      if (this.formUser.valid) {

        let data: FormData = new FormData();

        data.append('email', this.formUser.value.email == null ? '' : this.formUser.value.email);
        data.append('first_name', this.formUser.value.first_name == null ? '' : this.formUser.value.first_name);
        data.append('last_name', this.formUser.value.last_name == null ? '' : this.formUser.value.last_name);
        data.append('user_type',  '1');
        data.append('password',  this.formUser.value.password == null ? '' : this.formUser.value.password);
        data.append('confirm_password',  this.formUser.value.confirm_password == null ? '' : this.formUser.value.confirm_password);

        this.user.add(data).subscribe(res => {
          this.formUser.reset();
          this.formUser.controls.email.setErrors(null);
          this.formUser.controls.first_name.setErrors(null);
          this.formUser.controls.last_name.setErrors(null);
          this.formUser.controls.password.setErrors(null);
          this.formUser.controls.confirm_password.setErrors(null);

          let err:any = res;
  
          this._snackBar.open(err.message, err.status, {
            duration: 4000,
          });

          this.closeForm();
        },err => {
          this._snackBar.open(err.error.message, err.error.status, {
            duration: 4000,
          });
        });
      }
  }

  delete(id) {
    this.user.delete(id).subscribe(res => {
      this.getData();
      this._snackBar.open(res.message, res.status, {
        duration: 4000,
      });
    });
  }

  onReset() {
      this.formUser.reset();
  }

  getData() {
    this.user.all().subscribe(res => {
      this.dataSource = res;
    });
  }

  ngOnInit() {
    this.getData();
  }

}
