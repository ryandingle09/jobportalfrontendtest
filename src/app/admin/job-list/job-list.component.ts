import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { JobModel } from '../../shared';
import { JobService } from '../../shared/services/job.service';
import { AuthService } from '../../shared/services/auth.service';
// import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
  public form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  });

  public ListShow = true;
  public dataSource: JobModel[] = [];
  public displayedColumns: any[] = ['id', 'title', 'created_at', 'updated_at', 'action'];
  public user_id = this.auth.getUserToken();

  constructor(private service: JobService, private _snackBar: MatSnackBar, private auth: AuthService) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.service.search(filterValue).subscribe(res => {
      this.dataSource = res;
    });
  }

  showForm() {
    this.ListShow = false;
    this.onReset();
  }

  closeForm() {
    this.ListShow = true;
    this.getData();
  }

  onSubmit() {
      if (this.form.valid) {

        let data: FormData = new FormData();

        data.append('title', this.form.value.title == null ? '' : this.form.value.title);
        data.append('description', this.form.value.description == null ? '' : this.form.value.description);
        data.append('employer_id', this.user_id.id); //change value to logged user employer

        this.service.add(data).subscribe(res => {
          this.form.reset();
          this.form.controls.title.setErrors(null);
          this.form.controls.description.setErrors(null);

          this._snackBar.open(res.message, res.status, {
            duration: 4000,
          });

          this.closeForm();
        });
      }
  }

  delete(id) {
    this.service.delete(id).subscribe(res => {
      this.getData();
      this._snackBar.open(res.message, res.status, {
        duration: 4000,
      });
    });
  }

  onReset() {
      this.form.reset();
  }

  getData() {
    this.service.all().subscribe(res => {
      this.dataSource = res;
    });
  }

  ngOnInit() {
    this.getData();
  }

}
