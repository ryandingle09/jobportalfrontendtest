import { Component, OnInit } from '@angular/core';
import { ApplicantService } from '../../shared/services/applicant.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-item',
  templateUrl: './applicant-item.component.html',
  styleUrls: ['./applicant-item.component.css']
})
export class ApplicantItemComponent implements OnInit {

  public id: any = this.route.params['value']['id'];
  public email: any;
  public full_name: any;
  public job_id: any;
  public contact: any;
  public resume: any;
  public created_at: any;

  constructor(private app: ApplicantService, private route: ActivatedRoute) {}

  getData() {
    this.app.get(this.id).subscribe(data => {
      this.email = data.email
      this.full_name = data.full_name
      this.job_id = data.job_id
      this.contact = data.contact
      this.resume = data.resume
      this.created_at = data.created_at
    });
  }

  ngOnInit() {
    this.getData();
  }

}
