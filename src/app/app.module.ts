import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatToolbarModule, MatButtonModule } from '@angular/material';
import { AppComponent } from './app.component';
import { HeaderComponent, FooterComponent } from './shared';
import { HomeModule } from './home/home.module';
import { AuthModule } from './auth/auth.module';
import { JobModule } from './job/job.module';
import { AdminModule } from './admin/admin.module';
import { HttpClientModule }    from '@angular/common/http';
import { StorageService } from './shared/services/storage.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  // { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
  // { path: '**', redirectTo: '/', },  // Wildcard route for a 404 page
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    MatToolbarModule,
    MatButtonModule,
    HomeModule,
    AuthModule,
    JobModule,
    AdminModule,
    HttpClientModule
  ],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
